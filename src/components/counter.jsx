import React, { Component } from "react";
class Counter extends Component {
  componentDidUpdate(prevProps, prevState) {
    console.log("Prev Props", prevProps);
    console.log("Prev State", prevState);
    if (prevProps.counter.value != this.props.counter.value) {
      //ajax call and get new data from the server
      //WARNING! To be deprecated in React v17. Use componentDidMount instead.
      console.log("call server api");
    }
  }

  componentWillUnmount() {
    console.log("app cunter unmounted");
  }
  // state = {
  //   value: this.props.counter.value
  //   //tags: ["tag1", "tag2", "tag3"]
  //   //imageUrl: "https://picsum.photos/200"
  // };

  // //getting reference of counter object bind event handlers
  // constructor() {
  //   super();
  //   this.HandleIncrement = this.HandleIncrement.bind(this);
  // }

  // HandleIncrement = value => {
  //   this.setState({ value: this.state.value + 1 });
  //   console.log(value);
  // };

  styles = {
    fontSize: 50,
    fontWeight: "bold"
  };

  renderTags() {
    if (this.state.tags.length === 0) return <p>There are no tags</p>;
    return (
      <ul>
        {this.state.tags.map(tag => (
          <li key={tag}>{tag}</li>
        ))}
      </ul>
    );
  }

  //jsx always need a parent element if
  //we are using more than two control
  render() {
    // return <React.Fragment>{this.renderTags()}</React.Fragment>;

    console.log(this.props);
    return (
      <div className="row">
        <div className="col-1">
          {this.props.children}
          {/* showing children Component */}
          <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
        </div>
        <div className="col">
          <button
            onClick={() => this.props.onIncrement(this.props.counter)}
            className="btn btn-secondary btn-sm"
          >
            +
          </button>
          <button
            onClick={() => this.props.onDecrement(this.props.counter)}
            className="btn btn-secondary btn-sm m-2"
            disabled={this.props.counter.value === 0 ? "disabled" : ""}
          >
            -
          </button>

          <button
            onClick={() => this.props.onDelete(this.props.counter.id)}
            className="btn btn-danger btn-sm"
          >
            Delete
          </button>
        </div>
      </div>
    );
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.props.counter.value === 0 ? "warning" : "primary";
    return classes;
  }
  formatCount() {
    const { value: count } = this.props.counter;
    return count === 0 ? "Zero" : count;
  }
}

export default Counter;
