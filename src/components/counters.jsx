import React, { Component } from "react";
import Counter from "./counter";
class Counters extends Component {
  render() {
    return (
      <div>
        <button
          onClick={this.props.onReset}
          className="btn btn-primary btn-sm m-2"
        >
          Delete
        </button>
        {this.props.counters.map(counter => (
          <Counter
            key={counter.id}
            onDelete={this.props.onDelete}
            // onDecrement={this.props.onDecrement}
            onIncrement={this.props.onIncrement}
            onDecrement={this.props.onDecrement}
            // value={counter.value}
            // selected={true}
            // id={counter.id}
            //Pass a object rather than props different
            counter={counter}
          >
            {/* //children props */}
            {/* <h4>Counter #{this.props.counters.id}</h4> */}
          </Counter>
        ))}
      </div>
    );
  }
}

export default Counters;
