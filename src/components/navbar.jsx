import React, { Component } from "react";
//stateless component
//object destruction and using the properties which i am intrested in
const NavBar = ({ totalCounter }) => {
  console.log("navbar -rendered");
  return (
    <nav className="navbar navbar-light bg-light">
      <a className="navbar-brand" href="#">
        Navbar
        <span className="badge-pill badge-secondary m-6">{totalCounter}</span>
      </a>
    </nav>
  );
};

// class NavBar extends Component {
//   state = {};
//   render() {
//     return (
//       <nav className="navbar navbar-light bg-light">
//         <a className="navbar-brand" href="#">
//           Navbar
//           <span className="badge-pill badge-secondary m-6">
//             {this.props.totalCounter}
//           </span>
//         </a>
//       </nav>
//     );
//   }
// }
export default NavBar;
